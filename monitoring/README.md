Create Custom Prometheus:

1. apply prometheus.yaml

kubectl apply -n monitoring -f ./prometheus.yaml


2. apply servicemonitor.yaml

kubectl apply -n monitoring -f ./servicemonitor.yaml


3. deploy application
 
kubectl apply -n default -f ./python-app

4. Check metrics after port-forwarding Prometheus again. 
